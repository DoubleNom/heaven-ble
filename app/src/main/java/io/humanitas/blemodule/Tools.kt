package io.humanitas.blemodule

import android.annotation.SuppressLint
import android.content.Context
import android.os.ParcelUuid
import java.math.BigInteger
import java.nio.charset.Charset
import java.util.*
private const val SHARED_PREF_HEAVEN_CONFIG = "Heaven Config"
private const val PREF_HEAVEN_ADDRESS = "Heaven Address"


fun fromBytes(data: ByteArray): String {
    return String(data, Charset.forName("UTF-8"))
}

//* UUID
fun makeParcelUUID(uuid: String): ParcelUuid {
    return ParcelUuid.fromString(
            UUID.nameUUIDFromBytes(uuid.toByteArray()).toString()
    )
}

fun makeUUID(uuid: String): UUID {
    return UUID.nameUUIDFromBytes(uuid.toByteArray())
}


//* Heaven Address
private fun uuidToHeavenAddress(uuid: String): String {
    val c = CharArray(4)
    for (i in 0..3) {
        var value = BigInteger(uuid.substring(i * 8, i * 8 + 8), 16)
        value = value.mod(BigInteger.valueOf(36))
        if (value.toInt() < 26) {
            c[i] = (value.toInt() + 'a'.toInt()).toChar()
        } else {
            c[i] = (value.toInt() - 26 + '0'.toInt()).toChar()
        }
    }

    return String.format("%c%c%c%c", c[0], c[1], c[2], c[3])
}

@SuppressLint("ApplySharedPref")
fun getHeavenAddress(context: Context): String {
    val sharedPreferences = context.getSharedPreferences(SHARED_PREF_HEAVEN_CONFIG, Context.MODE_PRIVATE)

    var heavenAddress = sharedPreferences.getString(PREF_HEAVEN_ADDRESS, null)
    if (heavenAddress == null) {
        heavenAddress = UUID.randomUUID().toString().replace("-".toRegex(), "").toUpperCase()
        heavenAddress = uuidToHeavenAddress(heavenAddress)
        sharedPreferences.edit().putString(PREF_HEAVEN_ADDRESS, heavenAddress).commit()
    }
    return heavenAddress
}

//* Random string
fun generateRandomString(length: Int): String {
    val stringBuilder = StringBuilder()
    for (i in 0 until length) {
        stringBuilder.append((Random().nextInt(26) + 'a'.toInt()).toChar())
    }
    return stringBuilder.toString()
}

fun equals(s1: String?, s2: String?): Boolean {
    return if (s1 == null) s2 == null else s1 == s2
}