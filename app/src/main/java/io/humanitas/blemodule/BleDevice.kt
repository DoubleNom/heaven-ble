package io.humanitas.blemodule

import android.bluetooth.BluetoothDevice

internal class BleDevice(val address: String, val bluetoothDevice: BluetoothDevice) {
    val timestamp: Long = System.currentTimeMillis()

    override fun toString(): String {
        return String.format("%s-%s", address, bluetoothDevice.address)
    }
}
