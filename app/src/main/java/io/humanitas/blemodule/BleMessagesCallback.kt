package io.humanitas.blemodule

interface BleMessagesCallback {
    fun receivedMessage(sender: String, payload: String)
    fun onSendForgotten(message: Message): Boolean
    fun onSendFailure(message: Message, errorCode: Int): Boolean
}
