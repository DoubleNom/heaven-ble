package io.humanitas.blemodule

import no.nordicsemi.android.ble.BleManagerCallbacks

interface BleManagerCallbacks: BleManagerCallbacks {
    fun onSendSuccess()
    fun onSendFailure(errorCode: Int)
}