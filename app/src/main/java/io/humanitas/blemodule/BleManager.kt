package io.humanitas.blemodule

import io.humanitas.blemodule.BleMain.Companion.TAG
import io.humanitas.blemodule.Config.CHARACTERISTIC_DATA
import io.humanitas.blemodule.Config.SERVICE_DATA
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.content.Context
import android.util.Log
import no.nordicsemi.android.ble.BleManager


class BleManager(context: Context):
        BleManager<BleManagerCallbacks>(context) {
    override fun getGattCallback(): BleManagerGattCallback {
        return mGattCallback
    }

    private var c: BluetoothGattCharacteristic? = null

    private var perfMessageLength: Int = 0
    private var perfMessageStart: Long = 0

    fun send(message: String) {
        writeCharacteristic(c, message.toByteArray())
                .before {
                    if (perfMessageLength == 0) {
                        perfMessageLength = message.length * 8
                        perfMessageStart = System.currentTimeMillis()
                    }
                }
                .done { mCallbacks.onSendSuccess() }
                .fail { _: BluetoothDevice, i: Int -> mCallbacks.onSendFailure(i) }
                .invalid { send(message) }
                .split()
                .enqueue()

//        Log.d(TAG, "sending: $message")
    }

    private val mGattCallback: BleManagerGattCallback = object : BleManagerGattCallback() {
        override fun initialize() {
            super.initialize()
            requestMtu(80).enqueue()
            readCharacteristic(c).with { device, data ->
                Log.d(TAG, "read char: ${device.name} $data")
            }
//            overrideMtu(27) // Force requesting MTU, request was ignored on fast reconnect
//            requestMtu(517).enqueue()
        }

        override fun isRequiredServiceSupported(gatt: BluetoothGatt): Boolean {
            val service = gatt.getService(makeUUID(SERVICE_DATA))
            if (service != null) {
                c = service.getCharacteristic(makeUUID(CHARACTERISTIC_DATA))
                if (c == null) {
                    Log.d(TAG, "Characteristic not found")
                    // Calling hidden Android function. This is bad but fuck google for hiding it
                    gatt.javaClass.getMethod("refresh").invoke(gatt)
                }
            } else {
                Log.d(TAG, "Service not supported")
                // Calling hidden Android function. This is bad but fuck google for hiding it
                gatt.javaClass.getMethod("refresh").invoke(gatt)
            }
            return c != null
        }

        override fun onDeviceDisconnected() {
        }

    }



}