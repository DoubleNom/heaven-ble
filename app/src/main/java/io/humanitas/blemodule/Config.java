package io.humanitas.blemodule;

import android.bluetooth.le.ScanSettings;

class Config {
    static final String UUID_APP = "Humanitas";
    static final String SERVICE_DATA = "dataS";
    static final String CHARACTERISTIC_DATA = "dataC";
    static final String DESCRIPTOR_DATA = "dataD";
    static final long DEVICE_TIMEOUT =  2 * 60 * 1000L; // if device not seen after this time, we consider it not reachable
    static final long DEVICE_CHECKER = 4 * 1000L;       // 4 s
    static final int SCAN_MODE = ScanSettings.SCAN_MODE_BALANCED;
}
