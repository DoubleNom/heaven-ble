package io.humanitas.blemodule;

import org.jetbrains.annotations.NotNull;

import static io.humanitas.blemodule.ToolsKt.generateRandomString;

/**
 * Typical BLE Message
 * [Start char (1)][id (5)][sender (4)][recipient (4)][payload (variable)][End char (1)]
 */
public class Message {

    //* Message parsing point
    private static final int ID_START = 0;
    private static final int ID_SIZE = 5;
    private static final int SENDER_START = ID_START + ID_SIZE;
    private static final int SENDER_SIZE = 4;
    private static final int RECEIVER_START = SENDER_START + SENDER_SIZE;
    private static final int RECEIVER_SIZE = 4;
    private static final int PAYLOAD_START = RECEIVER_START + RECEIVER_SIZE;

    //* Message parsing kind
    private static final String START_STRING = String.format("%c", (char)2);
    private static final String END_STRING = String.format("%c", (char)3);

    //* Message size
    protected String id;
    protected String sender;
    protected String receiver;
    protected String payload;
    protected String message;
    private int failure;

    public Message() {

    }

    public Message(String sender, String receiver, String content) {
        this.id = generateRandomString(ID_SIZE);
        this.sender = sender;
        this.receiver = receiver;

        if(content != null) {
            this.payload = content;
        }

        this.message = getMessage();

        failure = 0;
    }

    public String getId() {
        return id;
    }

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    protected String getMessage() {
        return START_STRING + id + sender + receiver + payload + END_STRING;
    }

    public int increaseFailure() {
        return ++failure;
    }


    void addNextPart(String part) {
        if(message == null | (message != null && message.isEmpty())) {
            message = part;
        } else {
            message = message + part;
        }
    }

    static boolean isNewMessage(String part) {
        return START_STRING.charAt(0) == part.charAt(0);
    }

    boolean isComplete() {
        if(message.charAt(message.length() - 1) == END_STRING.charAt(0)) {
            parseMessage(message.substring(1, message.length() - 1));
            return true;
        } else {
            return false;
        }
    }

    private void parseMessage(String message) {
        id = message.substring(ID_START, ID_START + ID_SIZE);
        sender = message.substring(SENDER_START, SENDER_START + SENDER_SIZE);
        receiver = message.substring(RECEIVER_START, RECEIVER_START + RECEIVER_SIZE);
        payload = message.substring(PAYLOAD_START);
        this.message = message;
    }

    @NotNull
    public String toString() {
        return String.format("ID:%s S:%s R:%s P:%s", id, sender, receiver, payload);
    }
}
