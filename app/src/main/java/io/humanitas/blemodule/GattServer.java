package io.humanitas.blemodule;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.util.Log;

import java.util.UUID;

import static io.humanitas.blemodule.BleMain.TAG;
import static io.humanitas.blemodule.Config.CHARACTERISTIC_DATA;
import static io.humanitas.blemodule.Config.DESCRIPTOR_DATA;
import static io.humanitas.blemodule.Config.SERVICE_DATA;
import static io.humanitas.blemodule.ToolsKt.makeUUID;


public class GattServer {

    private final Context context;
    private final BluetoothManager mManager;
    private BluetoothGattServer mServer;
    private final BluetoothGattServerCallback mServerCallback;
    private BluetoothGattCharacteristic characteristic;


    GattServer
            (Context context, BluetoothManager manager, BluetoothGattServerCallback callback) {
        this.context = context;
        this.mManager = manager;
        this.mServerCallback = callback;
    }

    @SuppressWarnings("SameParameterValue")
    void sendResponse(android.bluetooth.BluetoothDevice device,
                      int requestId,
                      int status,
                      int offset,
                      byte[] value) {
        try {
            mServer.sendResponse(device,
                    requestId,
                    status,
                    offset,
                    value);
        } catch (NullPointerException e) {
            e.printStackTrace();
            Log.d(TAG, "sendResponse: failure\n" + e.getMessage());
            if(mServer == null) {
                Log.d(TAG, "sendResponse: mServer");
            }
            if(device == null) {
                Log.d(TAG, "sendResponse: device");
            }
            if(value == null) {
                Log.d(TAG, "sendResponse: value");
            }
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
            // This usually happen when device receive several messages at the same time
        }
    }

    public void startGattServer() {
        mServer = mManager.openGattServer(context, mServerCallback);

        BluetoothGattService service = new BluetoothGattService(
                makeUUID(SERVICE_DATA),
                BluetoothGattService.SERVICE_TYPE_PRIMARY
        );

        characteristic = new BluetoothGattCharacteristic(
                makeUUID(CHARACTERISTIC_DATA),
                Constants.GATT_SERVER.PROPERTIES,
                Constants.GATT_SERVER.PERMISSIONS
        );
        characteristic.setValue("characteristic".getBytes() );

        BluetoothGattDescriptor descriptor = new BluetoothGattDescriptor(
                makeUUID(DESCRIPTOR_DATA),
                Constants.GATT_SERVER.PERMISSIONS
        );
        descriptor.setValue("descriptor".getBytes());

        service.addCharacteristic(characteristic);

        mServer.addService(service);

        BluetoothGattService dummyService = new BluetoothGattService(
                UUID.fromString("00000000-9898-1000-8000-00805F9B34FB"),
                BluetoothGattService.SERVICE_TYPE_SECONDARY
        );

        BluetoothGattCharacteristic dummyChar = new BluetoothGattCharacteristic(
                UUID.fromString("00000000-8888-1000-8000-00805F9B34FB"),
                BluetoothGattCharacteristic.PROPERTY_READ,
                BluetoothGattCharacteristic.PERMISSION_READ
        );
        dummyChar.setValue("char");

        mServer.addService(dummyService);
    }
    public void stopGattServer() {
        if(mServer != null) {
            mServer.close();
            mServer = null;
        }
    }
}
