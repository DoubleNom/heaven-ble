package io.humanitas.blemodule;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;

public class Constants {


    public interface GATT_SERVER {
        int PROPERTIES =
            BluetoothGattCharacteristic.PROPERTY_NOTIFY |
            BluetoothGattCharacteristic.PROPERTY_READ |
            BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE;

        int PERMISSIONS =
            BluetoothGattCharacteristic.PERMISSION_READ |
            BluetoothGattCharacteristic.PERMISSION_WRITE;

        int PERMISSIONS_DESCRIPTOR =
                BluetoothGattDescriptor.PERMISSION_READ |
                BluetoothGattDescriptor.PERMISSION_WRITE;
    }
}
