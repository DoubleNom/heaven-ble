package io.humanitas.blemodule

import io.humanitas.blemodule.Config.UUID_APP
import android.bluetooth.BluetoothAdapter
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.BluetoothLeAdvertiser

internal class Advertiser(private val mAdapter: BluetoothAdapter, private val mAdCallback: AdvertiseCallback) {
    private var mAdvertiser: BluetoothLeAdvertiser? = null
    private var mAdSettings: AdvertiseSettings? = null
    private var mAdData: AdvertiseData? = null


    fun initAdvertiser(heavenAddress: String): Boolean {
        mAdvertiser = mAdapter.bluetoothLeAdvertiser

        if (mAdvertiser == null) {
            return true
        }

        mAdSettings = AdvertiseSettings.Builder()
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH)
                .setConnectable(true)
                .build()

        mAdData = AdvertiseData.Builder()
                .addServiceUuid(makeParcelUUID(UUID_APP))
//                .addServiceData(makeParcelUUID(UUID_APP), "1234".toByteArray())
                .setIncludeDeviceName(true)
                .setIncludeTxPowerLevel(true)
                .build()

        return false
    }

    fun startAdvert() {
        if (mAdvertiser == null) return
        mAdvertiser!!.startAdvertising(mAdSettings, mAdData, mAdCallback)
    }

    fun stopAdvert() {
        mAdvertiser!!.stopAdvertising(mAdCallback)
    }
}
