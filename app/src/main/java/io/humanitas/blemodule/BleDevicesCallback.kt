package io.humanitas.blemodule

interface BleDevicesCallback {
    fun newDevice(address: String)
    fun removeDevice(address: String)
}
