package io.humanitas.blemodule

import io.humanitas.blemodule.Config.DEVICE_CHECKER
import io.humanitas.blemodule.Config.DEVICE_TIMEOUT
import android.bluetooth.BluetoothDevice
import android.os.Handler

class Devices (private val deviceCallback: BleDevicesCallback) {
    private val handler: Handler = Handler()
    private var devices: ArrayList<BleDevice> = ArrayList()
    private val deviceRunnable = Runnable { devicesRefresh() }

    fun start() {
        devices.clear()
        handler.removeCallbacksAndMessages(null)
        handler.post(deviceRunnable)
    }

    fun stop() {
        handler.removeCallbacksAndMessages(null)
    }

    fun getDevice(address: String): BluetoothDevice? {
        val index = getDevicesIndex(address)
        return if (index == -1) null else devices[index].bluetoothDevice
    }

    internal fun updateList(address: String, device: BluetoothDevice) {
        val index = getDevicesIndex(address)
        if (index == -1) {
            if (addDevice(address, device)) {
                deviceCallback.newDevice(address)
            }
        } else {
            // check if last update wasn't too recent
            updateDevice(index, address, device)
        }
    }

    private fun updateDevice(index: Int, address: String, device: BluetoothDevice) {
        if (index == -1) return
        devices[index] = BleDevice(address, device)
    }

    private fun addDevice(address: String, device: BluetoothDevice): Boolean {
        if (isInList(address)) return false
        devices.add(BleDevice(address, device))
        return true
    }

    private fun isInList(address: String): Boolean {
        for (device in devices) {
            if (address == device.address) {
                return true
            }
        }
        return false
    }

    private fun getDevicesIndex(address: String): Int {
        for (i in devices.indices) {
            if (devices[i].address == address) {
                return i
            }
        }
        return -1
    }

    private fun devicesRefresh() {
        val i = devices.iterator()
        for (device in i) {
            if (System .currentTimeMillis() - device.timestamp > DEVICE_TIMEOUT) {
                deviceCallback.removeDevice(device.address)
                i.remove()
            }
        }
        handler.postDelayed(deviceRunnable, DEVICE_CHECKER)
    }

    fun remove(target: BluetoothDevice) {
        val i = devices.iterator()
        for (device in i) {
            if (target.address == device.address) {
                deviceCallback.removeDevice(device.address)
                i.remove()
            }
        }
    }

    override fun toString(): String {
        return devices.toString()
    }
}
