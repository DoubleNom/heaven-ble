package io.humanitas.blemodule

import android.os.ParcelUuid
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat
import no.nordicsemi.android.support.v18.scanner.ScanCallback
import no.nordicsemi.android.support.v18.scanner.ScanFilter
import no.nordicsemi.android.support.v18.scanner.ScanSettings
import java.util.UUID

class BleScanner {
    private var callback: ScanCallback? = null
    private var scanner: BluetoothLeScannerCompat = BluetoothLeScannerCompat.getScanner()
    private var scanSettings: ScanSettings = ScanSettings.Builder()
                                            .setScanMode(Config.SCAN_MODE)
                                            .build()
    private var scanFilters = ArrayList<ScanFilter>()

    init {
        @Suppress("SpellCheckingInspection")
        scanFilters.add(ScanFilter.Builder()
//                For iOS compatibility testing
//                .setServiceUuid(ParcelUuid.fromString("6E400001-B5A3-F393-E0A9-E50E24DCCA9E"))
//                For Humanitas service
                .setServiceUuid(ParcelUuid.fromString(
                                UUID.nameUUIDFromBytes(Config.UUID_APP.toByteArray()).toString()))
                .build())
    }

    fun startScan(callback: ScanCallback) {
        if (this.callback != null) return
        scanner.startScan(
                scanFilters,
                scanSettings,
                callback
        )
        this.callback = callback
    }

    fun stopScan() {
        if (callback == null) return
        scanner.stopScan(callback!!)
        callback = null
    }

}