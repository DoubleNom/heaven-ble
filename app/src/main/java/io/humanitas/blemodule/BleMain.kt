package io.humanitas.blemodule

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothAdapter.ACTION_LOCAL_NAME_CHANGED
import android.bluetooth.BluetoothAdapter.ACTION_STATE_CHANGED
import android.bluetooth.BluetoothAdapter.EXTRA_LOCAL_NAME
import android.bluetooth.BluetoothAdapter.EXTRA_STATE
import android.bluetooth.BluetoothAdapter.STATE_OFF
import android.bluetooth.BluetoothAdapter.STATE_ON
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattServerCallback
import android.bluetooth.BluetoothManager
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseSettings
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Handler
import android.os.Looper
import android.util.Log
import no.nordicsemi.android.support.v18.scanner.ScanCallback
import no.nordicsemi.android.support.v18.scanner.ScanResult


class BleMain   (
        private val context: Context,
        maxTypedMessage: Int,
        devicesCallback: BleDevicesCallback,
        private val msgCallback: BleMessagesCallback
) {

    // This device heaven address
    private val thisAddress = getHeavenAddress(context)

    // Android bluetooth manager
    private var blManager: BluetoothManager =
            context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
    // Android bluetooth adapter
    private var blAdapter: BluetoothAdapter = blManager.adapter
    // Extended nordic bluetooth manager
    private var manager: BleManager? = null
    // Modified android gatt server
    private var server: GattServer? = null
    // Modified nordic bluetooth scanner
    private var scanner: BleScanner? = null
    // Modified Android bluetooth advertiser
    private var advertiser: Advertiser? = null
    // Bluetooth devices manager
    private var devices = Devices(devicesCallback)
    // Bluetooth messages buffer
    private val bleMessagesBuffer: BleMessagesBuffer = BleMessagesBuffer(maxTypedMessage)

    // Bluetooth broadcast receiver
    private var blReceiver: BroadcastReceiver? = null


    // mSending process
    private var mSending: Boolean = false
    private var mMessage: Message? = null

    private val myHandler: Handler = Handler(Looper.getMainLooper())

    fun startBle(): Boolean {
        if (blReceiver == null) {
            blReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context?, intent: Intent?) {
                    if (intent == null) return

                    when (intent.action) {
                        ACTION_LOCAL_NAME_CHANGED -> {
                            Log.d(TAG, "BL name: ${intent.getStringExtra(EXTRA_LOCAL_NAME)}")
                            startFeatures()
                        }
                        ACTION_STATE_CHANGED -> {
                            when (intent.getIntExtra(EXTRA_STATE, -1)) {
                                STATE_ON -> {
                                    Log.d(TAG, "BL is ON")
                                    myHandler.postDelayed({
                                        startFeatures()
                                    }, 100)
                                }
                                STATE_OFF -> {
                                    Log.d(TAG, "BL is OFF")
                                }
                            }
                        }
                    }
                }
            }
            val filter = IntentFilter()
            filter.addAction(ACTION_LOCAL_NAME_CHANGED)
            filter.addAction(ACTION_STATE_CHANGED)
            context.registerReceiver(blReceiver, filter)
        }

        blManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        blAdapter = blManager.adapter

        devices.start()

        mSending = false
        mMessage = null

        return if (blAdapter.isEnabled) {
            startFeatures()
            true
        } else {
            blAdapter.enable()
            false
        }
    }

    private fun startFeatures () {
        if (!blAdapter.isEnabled) return

        myHandler.postDelayed({
                if (manager == null) {
                    manager = BleManager(context)
                    manager!!.setGattCallbacks(gattCallback)
                }

                if (server == null) {
                    server = GattServer(context, blManager, gattServerCallback)
                    server!!.startGattServer()
                }

                if (scanner == null) {
                    scanner = BleScanner()
                    scanner!!.startScan(scanCallback)
                }

                if (advertiser == null) {
                    if (blAdapter.name != thisAddress) {
                        blAdapter.name = thisAddress
                    } else {
                        advertiser = Advertiser(
                                blAdapter,
                                advertCallback
                        )
                        advertiser!!.initAdvertiser(thisAddress)
                        advertiser!!.startAdvert()
                    }
                }
        }, 100)
    }

    fun connect(address: String) {
        val device = devices.getDevice(address)
        if (device == null) {
            Log.d(TAG, "No device for $address")
        } else {
            manager!!.connect(device)
                    .retry(3, 100)
                    .useAutoConnect(false)
                    .fail { _: BluetoothDevice, _: Int -> recoverSending() }
                    .enqueue()
        }
    }

    fun send(target: String, payload: String, type: Int = 0, requestSend: Boolean = true) {
        send(Message(thisAddress, target, payload), type, requestSend)
    }

    fun send(message: Message, type: Int = 0, requestSend: Boolean = true) {
        bleMessagesBuffer.addMessage(message, type)
        if (requestSend) requestSending()
    }

    private fun requestSending(force: Boolean = false) {
        if (!mSending || force) {
            mSending = true
            if (bleMessagesBuffer.hasNextPeer()) {
                connect(bleMessagesBuffer.nextPeer()!!)
            } else {
                mSending = false
            }
        }
    }

    private fun trySending() {
        if (mSending) {
            if (bleMessagesBuffer.hasNextMessage()) {
                mMessage = bleMessagesBuffer.nextMessage()
                manager!!.send(mMessage!!.getMessage())
            } else {
                disconnect()
            }
        }
    }

    private fun recoverSending() {
        mSending = false
        disconnect()
    }

    private fun disconnect() {
        myHandler.post {
            manager?.disconnect()?.enqueue()
        }
    }

    fun stopBle() {
        if (advertiser != null) {
            advertiser!!.stopAdvert()
            advertiser = null
        }

        if (scanner != null) {
            scanner!!.stopScan()
            scanner = null
        }

        if (server != null) {
            server!!.stopGattServer()
            server = null
        }

        devices.stop()
        blAdapter.disable()
        if (blReceiver != null) {
            context.unregisterReceiver(blReceiver)
            blReceiver = null
        }
    }

    // Bluetooth callbacks
    private val scanCallback: ScanCallback = object : ScanCallback() {
        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
            Log.d(TAG, "BLE Scan failed: $errorCode")
        }

        override fun onScanResult(callbackType: Int, result: ScanResult) {
            super.onScanResult(callbackType, result)

            val address = result.scanRecord?.deviceName

            if (address != null) {
                devices.updateList(address, result.device)
            }
        }
    }

    private val advertCallback: AdvertiseCallback = object : AdvertiseCallback() {
        override fun onStartSuccess(settingsInEffect: AdvertiseSettings?) {
            super.onStartSuccess(settingsInEffect)
            Log.d(TAG, "advertising started")
        }

        override fun onStartFailure(errorCode: Int) {
            super.onStartFailure(errorCode)
            Log.d(TAG, "advertising failed: $errorCode")
        }
    }

    private val gattCallback: BleManagerCallbacks = object : BleManagerCallbacks {
        override fun onDeviceConnecting(device: BluetoothDevice) { }

        override fun onDeviceConnected(device: BluetoothDevice) { }

        override fun onDeviceReady(device: BluetoothDevice) {
            myHandler.post { trySending() }
        }

        override fun onServicesDiscovered(device: BluetoothDevice, optionalServicesFound: Boolean) { }

        override fun onBondingRequired(device: BluetoothDevice) { }

        override fun onBonded(device: BluetoothDevice) { }

        override fun onSendSuccess() {
            myHandler.post { trySending() }
        }

        override fun onSendFailure(errorCode: Int) { }

        override fun onDeviceDisconnecting(device: BluetoothDevice) { }

        override fun onDeviceDisconnected(device: BluetoothDevice) {
            if (mSending) myHandler.post { requestSending(true) }
        }

        override fun onDeviceNotSupported(device: BluetoothDevice) {
            myHandler.post {
                Log.d(TAG, "${device.name} - ${device.address} does not have required services")
                disconnect()
                devices.remove(device)
            }
        }

        override fun onBondingFailed(device: BluetoothDevice) { }

        override fun onLinkLossOccurred(device: BluetoothDevice) {
            myHandler.post { recoverSending() }
        }

        override fun onError(device: BluetoothDevice, message: String, errorCode: Int) {
            myHandler.post { recoverSending() }
        }
    }

    private val gattServerCallback: BluetoothGattServerCallback =
            object : BluetoothGattServerCallback() {
                var messages: HashMap<String, Message> = HashMap()

                override fun onCharacteristicWriteRequest(
                        device: BluetoothDevice?,
                        requestId: Int,
                        characteristic: BluetoothGattCharacteristic?,
                        preparedWrite: Boolean,
                        responseNeeded: Boolean,
                        offset: Int,
                        value: ByteArray?
                ) {
                    super.onCharacteristicWriteRequest(
                            device,
                            requestId,
                            characteristic,
                            preparedWrite,
                            responseNeeded,
                            offset,
                            value
                    )

                    if (responseNeeded) {
                        server!!.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, offset, value)
                    }

                    if (value == null || device == null || device.address == null) return

                    // parse byte array to string
                    val str = fromBytes(value)
                    Log.d(TAG, "${device.address}: $str")

                    // check if value is the beginning of a new message
                    if (Message.isNewMessage(str)) {
                        messages[device.address] = Message()
                    } else if(messages[device.address] == null)  {
                        // message is not a new message but we don't have any slot ready for it
                        // this is irregular, we reject it
                        return
                    }

                    // add part of the message to the device reserved slot
                    messages[device.address]!!.addNextPart(str)

                    // check if message is complete (has beginning and end char in it)
                    if (messages[device.address]!!.isComplete) {
                        // TODO: add ACK

                        // notify that a message was received
                        val messageReceived = messages[device.address]!!
                        msgCallback.receivedMessage(messageReceived.sender, messageReceived.payload)

                        // remove the device slot from reception slot
                        messages.remove(device.address)
                    }
                }
            }

    fun devicesToString(): String {
        return devices.toString()
    }

    companion object {
        const val TAG = "BLE"

        @JvmStatic
        fun doKill(context: Context) {
            val manager: BluetoothManager = context
                    .applicationContext
                    .getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            val adapter: BluetoothAdapter = manager.adapter
            adapter.disable()
        }
    }
}
