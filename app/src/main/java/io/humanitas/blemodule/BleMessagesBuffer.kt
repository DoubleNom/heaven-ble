package io.humanitas.blemodule

class BleMessagesBuffer(var maxTypedValue: Int) {
    private var peersSendingList: LinkedHashMap<String, LinkedHashMap<Int, Message>> = LinkedHashMap()
    private var currentSendingKey: String? = null
    private var currentSendingList: LinkedHashMap<Int, Message> = LinkedHashMap()
    private var untypedCounter: Int = 0

    init {
        if (maxTypedValue < 0) maxTypedValue = 0
        if (maxTypedValue != 0) untypedCounter = maxTypedValue
    }

    private fun getNextUntypedValue(): Int {
        if (Int.MAX_VALUE == untypedCounter) {
            untypedCounter = maxTypedValue
        }
        return ++untypedCounter
    }

    /**
     * Add a message to the buffer
     * @param message the message to add to the buffer
     * @param type the type of the message if needed, typed message will be overwritten if a new one is inputted before
     * sending
     */
    fun addMessage(message: Message, type: Int = 0) {
        val peerAddress = message.receiver
        var peerSendingList = peersSendingList[peerAddress]
        if (null == peerSendingList) {
            peerSendingList = LinkedHashMap()
            peersSendingList[peerAddress!!] = peerSendingList
        }
        if (0 == type) {
            peerSendingList[getNextUntypedValue()] = message
        } else  {
            peerSendingList[type] = message
        }
    }

    /**
     * Tell if there are peers to which we have to send message
     * @return Return true if there are peers on the sending list or if the current sending list is not empty
     */
    fun hasNextPeer(): Boolean {
        return !peersSendingList.isEmpty() || !currentSendingList.isEmpty()
    }

    /**
     * Tell what is the address of the next peer in line for the sending
     * @return: Return the address of the next peer if there is one, return null otherwise
     */
    fun nextPeer(): String? {
        if (!currentSendingList.isEmpty()) {
            for (entry in currentSendingList.entries) {
                addMessage(entry.value, entry.key)
            }
        }

        return if (!peersSendingList.isEmpty()) {
            // current list still has messages to send
            // We stock it back to the buffer

            // Find next peer in list and pop it from sending list
            currentSendingKey  = peersSendingList.keys.first()
            currentSendingList = peersSendingList[currentSendingKey!!]!!
            peersSendingList.remove(currentSendingKey!!)
            currentSendingKey
        } else {
            null
        }
    }

    /**
     * Tell if current buffer still has message to send for the current peer
     * @return Return true if there are message to send
     */
    fun hasNextMessage(): Boolean {
        return !currentSendingList.isEmpty()
    }


    /**
     * Return the next message to send for the current peer
     * @return Return the message to send
     */
    fun nextMessage(): Message? {
        return if (!currentSendingList.isEmpty()) {
            val key: Int = currentSendingList.keys.first()
            val message = currentSendingList[key]
            currentSendingList.remove(key)
            message
        } else {
            null
        }
    }

//  Not used yet
//    /**
//     * Remove a typed message for all peers if it exist
//     * @param type The type of the message we want to erase
//     */
//    fun removeMessageType(type: Int) {
//        val iterator = peersSendingList.iterator()
//        for (i in iterator) {
//            i.value.remove(type)
//            if(i.value.isEmpty()) {
//                iterator.remove()
//            }
//        }
//    }
//
//    /**
//     * Remove a typed message for a specific peer if it exist
//     * @param address The peer we target
//     * @param
//     */
//    fun removeMessageType(address: String, type: Int) {
//        val peerSendingList = peersSendingList[address]
//        if (peerSendingList != null) {
//            peerSendingList.remove(type)
//            if (peerSendingList.isEmpty()) peersSendingList.remove(address)
//        }
//
//        if (currentSendingKey == address) {
//            currentSendingList.remove(type)
//        }
//    }
//
//    fun removePeer(address: String) {
//        peersSendingList.remove(address)
//
//        if (currentSendingKey == address) {
//            currentSendingList.clear()
//        }
//    }

    override fun toString(): String {
        return peersSendingList.toString()
    }
}
